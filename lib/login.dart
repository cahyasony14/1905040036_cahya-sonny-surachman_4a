import 'package:flutter/material.dart';
import 'package:uts_mobpro/home.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key, required String title}) : super(key: key);

 @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login Page"),
      ),
    body: Padding(
      padding: const EdgeInsets.all(24.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children:[
        Text(
          "Email",
          ),
        SizedBox(
          height: 10.0,
        ),
        TextField(
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: "Enter your email...",
          ),
        ),
        SizedBox(
          height: 20.0,  
        ),
           Text(
          "Password",
          ),
            SizedBox(
          height: 10.0,
        ),
        TextField(
          obscureText: true,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: "Enter your password...",
          ),
        ),
          SizedBox(
          height: 20.0,
        ),
        SizedBox(
          width: double.infinity,
          height: 50.0,
          child: ElevatedButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage(),
            ),
            );
          }, 
          child: Text("Login"),
          ),
        ),
      ]),
    ),
    );
  }
}
