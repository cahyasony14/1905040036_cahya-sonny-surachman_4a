import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Home Page",
          ),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Selamat Datang di Home Page!"),
                ElevatedButton.icon(
                  style: ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.red)),
                  onPressed: (){}, 
                  icon: Icon(Icons.logout_outlined),
                 label: Text("Logout!"),
                 ),
              ],
            ),
          ),
    );
  }
} 