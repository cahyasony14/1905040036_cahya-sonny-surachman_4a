import 'package:flutter/material.dart';
import 'login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Dark Mode',
      themeMode: ThemeMode.dark ,
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      
      home: LoginPage(title: 'Flutter Dark Mode Login'
      ),
     
    );
  }
}